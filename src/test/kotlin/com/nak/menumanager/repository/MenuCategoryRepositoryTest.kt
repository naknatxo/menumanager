package com.nak.menumanager.repository

import com.nak.menumanager.MenumanagerApplication
import com.nak.menumanager.repository.data.MenuCategory
import io.kotlintest.specs.StringSpec
import io.kotlintest.spring.SpringListener
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotNull
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.annotation.DirtiesContext
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import reactor.test.StepVerifier
import java.util.*

@SpringBootTest(classes = [MenumanagerApplication::class])
@DirtiesContext
class MenuCategoryRepositoryTest : StringSpec() {

    override fun listeners() = listOf(SpringListener)

    @Autowired
    private lateinit var menuCategoryRepository: MenuCategoryRepository

    init {

        "Should CRUD a menu category" {

            //***** Save
            var saved : Mono<MenuCategory>
            val name : String = "vegan_test"
            var menuCategory : MenuCategory = MenuCategory(name = name, description = "mydescription")
            saved = menuCategoryRepository.save(menuCategory)
            StepVerifier
                    .create(saved)
                    .assertNext { category -> assertNotNull(category.id) }
                    .expectComplete()
                    .verify()

            var result : Flux<MenuCategory> = menuCategoryRepository.findByName(name)
            result.log().subscribe { println("category saved ${it}") }

            //***** Read
            var found : Optional<MenuCategory> = result.toStream().filter { it.name.equals(name) }.findFirst()

            //***** Update
            var menuToChange : MenuCategory = found.get()
            menuToChange.description = "mydescription_changed"
            saved = menuCategoryRepository.save(menuToChange)
            StepVerifier
                    .create(saved)
                    .assertNext { category -> assertEquals(menuToChange.description, category.description) }
                    .expectComplete()
                    .verify()
            saved.log().subscribe{println("Item after chage ${it}")}

            //***** Delete
            saved.flatMap { menuCategoryRepository.delete(it) }
            found.get().id?.let { id ->
                println("Borradno:: "+id)
                menuCategoryRepository.deleteById(id).log().subscribe()
            }

        }

    }
}