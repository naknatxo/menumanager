import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {

	val springBootVersion = "2.1.7.RELEASE"
	val springdependecyManagementVersion = "1.0.8.RELEASE"
	val kotlinVersion = "1.3.41"

	id("org.springframework.boot") version springBootVersion
	id("io.spring.dependency-management") version springdependecyManagementVersion
	kotlin("plugin.jpa") version kotlinVersion
	kotlin("jvm") version kotlinVersion
	kotlin("plugin.spring") version kotlinVersion
}

group = "com.nak"
version = "0.0.1-SNAPSHOT"
java.sourceCompatibility = JavaVersion.VERSION_1_8

repositories {
	mavenCentral()
	maven(url = "https://repo.spring.io/milestone")
}

val kotlinVersion = "1.3.41"
val kotlinCorutinesVersion = "1.2.2"
val kotlinTestVersion = "3.4.1"
val reactorTestVersion = "3.2.11.RELEASE"

dependencies {

	implementation("org.springframework.boot:spring-boot-starter-actuator")
	implementation("org.springframework.boot:spring-boot-starter-webflux")

	implementation("org.springframework.data:spring-data-r2dbc:1.0.0.M1")
	implementation("io.r2dbc:r2dbc-postgresql:1.0.0.M6")
	implementation("io.projectreactor:reactor-core")

	//  Reactive Relational Database Connectivity
	implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
	implementation("org.jetbrains.kotlin:kotlin-reflect:${kotlinVersion}")
	implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8:${kotlinVersion}")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:${kotlinCorutinesVersion}")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-reactor:${kotlinCorutinesVersion}")
	//runtimeOnly("com.h2database:h2")

	testImplementation("io.projectreactor:reactor-test:${reactorTestVersion}")
	// KotlinTest
	testCompile("org.springframework.boot:spring-boot-starter-test") {
		exclude(module = "junit")
	}
	testImplementation("org.junit.jupiter:junit-jupiter-api")
	testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine")
	testImplementation("io.kotlintest:kotlintest-runner-junit5:${kotlinTestVersion}")
	testImplementation("io.kotlintest:kotlintest-extensions-spring:${kotlinTestVersion}")
}

tasks.withType<Test> {
	useJUnitPlatform()
}

tasks.withType<KotlinCompile> {
	kotlinOptions {
		freeCompilerArgs = listOf("-Xjsr305=strict")
		jvmTarget = "1.8"
	}
}
