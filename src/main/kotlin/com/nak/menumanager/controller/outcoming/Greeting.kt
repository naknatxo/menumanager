package com.nak.menumanager.controller.outcoming

data class Greeting(val id: Long, val content: String)