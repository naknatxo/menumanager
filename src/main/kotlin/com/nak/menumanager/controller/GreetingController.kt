package com.nak.menumanager.controller
import com.nak.menumanager.controller.outcoming.Greeting
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import reactor.core.publisher.Flux
import java.util.concurrent.atomic.AtomicLong
import reactor.core.publisher.Operators.`as`
import reactor.core.publisher.Mono



@RestController
class GreetingController {

    val counter = AtomicLong()

    @GetMapping("/greeting")
    fun greeting(@RequestParam(value = "name", defaultValue = "World") name: String) =
            Greeting(counter.incrementAndGet(), "Hello, $name")

    @GetMapping("/greeting2")
    fun greeting2(@RequestParam(value = "name", defaultValue = "World") name: String) :Greeting {
        return Greeting(counter.incrementAndGet(), "Hello, $name")
    }

    @GetMapping("/greetingMono")
    fun getMono(@RequestParam(value = "name", defaultValue = "World") name: String): Mono<Greeting> {
        return Mono.just(Greeting(counter.incrementAndGet(), "Hello, $name"))
    }

    @GetMapping("/greetingflux")
    fun getFlux(@RequestParam(value = "name", defaultValue = "World") name: String): Flux<Greeting> {
        println("hola")
        return Flux.just(Greeting(counter.incrementAndGet(), "Hello, $name"))
    }
}