package com.nak.menumanager.repository.data

import org.springframework.data.annotation.Id
import org.springframework.data.relational.core.mapping.Column
import org.springframework.data.relational.core.mapping.Table


@Table(value = "menu_category")
data class MenuCategory(

        @Id
        @Column(value = "menu_category_id")
        var id: Long? = null,

        @Column(value = "name")
        var name: String,

        @Column(value = "description")
        var description: String

) {
        override fun toString(): String = name + description + id
}