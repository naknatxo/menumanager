package com.nak.menumanager.controller

import com.nak.menumanager.repository.data.MenuCategory
import kotlinx.coroutines.flow.Flow
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.reactive.function.client.WebClient
import reactor.core.publisher.Flux

@RestController
class WebClientController () {

    private val webClient : WebClient = WebClient.create("http://localhost:8080")

    @GetMapping("/categorywbeclient")
    fun findAll() : Flux<MenuCategory> { // Flow<MenuCategory>

        val result = webClient.get()
                .uri("/category")
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .bodyToFlux(MenuCategory::class.java)

        result.log().subscribe { println("category ${it.name}") }
        return result
    }
}