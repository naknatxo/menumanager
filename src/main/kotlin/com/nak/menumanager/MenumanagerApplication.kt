package com.nak.menumanager

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class MenumanagerApplication

fun main(args: Array<String>) {
	runApplication<MenumanagerApplication>(*args)
}
