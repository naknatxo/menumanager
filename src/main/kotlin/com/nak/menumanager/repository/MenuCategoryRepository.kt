package com.nak.menumanager.repository

import com.nak.menumanager.repository.data.MenuCategory
import org.springframework.data.r2dbc.repository.R2dbcRepository
import org.springframework.data.r2dbc.repository.query.Query
import org.springframework.stereotype.Repository
import reactor.core.publisher.Flux

@Repository
interface MenuCategoryRepository : R2dbcRepository<MenuCategory, Long> {

    @Query("select menu_category_id, name, description from menu_category c where c.name = $1")
    fun findByName(name: String): Flux<MenuCategory>
}