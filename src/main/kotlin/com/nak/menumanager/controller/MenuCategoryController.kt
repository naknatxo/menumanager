package com.nak.menumanager.controller

import com.nak.menumanager.repository.MenuCategoryRepository
import com.nak.menumanager.repository.data.MenuCategory
import org.springframework.web.bind.annotation.*
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

@RestController
class MenuCategoryController (private val menuCategoryRepository : MenuCategoryRepository) {

    @PostMapping("/category")
    fun create(@RequestBody menuCategory: MenuCategory) : Mono<MenuCategory> {
        return menuCategoryRepository.save(menuCategory)
    }

    @PutMapping("/category/{id}")
    fun edit(@PathVariable id: Long,
             @RequestBody menuCategory: MenuCategory) : Mono<MenuCategory> {

        return menuCategoryRepository.findById(id).flatMap {
            it.description =  menuCategory.description
            menuCategoryRepository.save(it)
        }
    }

    @DeleteMapping("/category/{id}")
    fun delete(@PathVariable id: Long) : Mono<Void> {

        return menuCategoryRepository.findById(id)
                .flatMap {
                    menuCategoryRepository.delete(it)
                }
    }

    @GetMapping("/category")
    fun findAllFlux() : Flux<MenuCategory> {
        return menuCategoryRepository.findAll()
    }

}